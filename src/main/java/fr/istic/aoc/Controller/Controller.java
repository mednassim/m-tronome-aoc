package fr.istic.aoc.Controller;

import fr.istic.aoc.v1.ihm.ControllerIHM;

public interface Controller {

    public void dec();

    public void inc();

    public void start();

    public void stop();

    public void flashLed(int numLED);

    public void setIHM(ControllerIHM ihm);

}
