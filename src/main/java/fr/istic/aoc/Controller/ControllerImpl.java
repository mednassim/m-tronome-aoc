package fr.istic.aoc.Controller;

import fr.istic.aoc.Command.BipDelaiCmd;
import fr.istic.aoc.Command.BipPeriodCmd;
import fr.istic.aoc.Command.Command;
import fr.istic.aoc.Moteur.Engine;
import fr.istic.aoc.Moteur.EngineImpl;
import fr.istic.aoc.v1.ihm.ControllerIHM;

public class ControllerImpl implements Controller {

    private Engine engine;

    private ControllerIHM controllerIHM;

    public ControllerImpl() {

        engine = new EngineImpl();
        engine.setBpm(120);
        engine.setMesure(4);
    }

    public void setIHM(ControllerIHM ihm) {
        controllerIHM = ihm;
    }

    public void dec() {
        engine.decMesure();
        stop();
        start();

    }


    public void inc() {
        engine.incMesure();
        stop();
        start();

    }

    public void start() {
        if (!engine.isRunning()) {
            engine.setRunning(true);
            Command bipCmd = new BipPeriodCmd(this);
            Command mesureCmd = new BipDelaiCmd(this);
            engine.setBpm(Math.toIntExact((long) ((60.0 / controllerIHM.getSliderPosition()) * 1000)));

            engine.startMesure(mesureCmd);
            engine.startBip(bipCmd);

        }
    }


    public void stop() {
        if (engine.isRunning()) {
            engine.setRunning(false);
            engine.getHorloge().desactiver();
        }
    }

    public void flashLed(int numLED) {
        this.controllerIHM.signalLed(numLED);
    }

}
