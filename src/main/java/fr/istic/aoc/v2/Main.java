package fr.istic.aoc.v2;


import fr.istic.aoc.Controller.Controller;
import fr.istic.aoc.Controller.ControllerImpl;
import fr.istic.aoc.Moteur.Engine;
import fr.istic.aoc.Moteur.EngineImpl;
import fr.istic.aoc.adapter.ConcreteAdapter;
import fr.istic.aoc.v2.ihm.IHMImpl;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {

        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        IHMImpl ihm = new IHMImpl();
        Controller controller = new ControllerImpl();

        Engine moteur = new EngineImpl();
        ConcreteAdapter adapter = new ConcreteAdapter();

        controller.setIHM(adapter);
        adapter.setController(controller);

        ihm.setAdapter(adapter);
        adapter.setIHM(ihm);
        primaryStage.setTitle("Métronome");
        primaryStage.setScene(new Scene(ihm));
        primaryStage.sizeToScene();
        primaryStage.show();

    }
}
