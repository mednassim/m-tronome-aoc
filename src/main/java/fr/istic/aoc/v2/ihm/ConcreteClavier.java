package fr.istic.aoc.v2.ihm;


import fr.istic.aoc.Material.Clavier;

public class ConcreteClavier implements Clavier {
	IHMImpl ihm;
	
	public ConcreteClavier(IHMImpl ihm){
		this.ihm=ihm;
	}
	
	@Override
	public boolean touchePressée(int i) {
		return ihm.tabButton[i].isActive();
	}

}
