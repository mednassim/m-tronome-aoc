package fr.istic.aoc.v2.ihm;


import fr.istic.aoc.Material.Afficheur;
import javafx.animation.FillTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class ConcreteAfficheur implements Afficheur {

    private IHMImpl ihm;

    public ConcreteAfficheur(IHMImpl ihm) {
        this.ihm = ihm;
    }

    @Override
    public void allumerLED(int numLED) {
        new Thread(() -> {
            if (numLED == 1) {
                FillTransition st = new FillTransition(Duration.millis(100), ihm.getLed1(),
                        Color.WHITE, Color.RED);
                st.play();
            } else {
                FillTransition st = new FillTransition(Duration.millis(100), ihm.getLed2(),
                        Color.WHITE, Color.GREEN);
                st.play();
            }
        }).start();
    }


    @Override
    public void afficherTempo(int valeurTempo) {
        System.out.println(String.valueOf(valeurTempo));
        ihm.getScreen().setText(String.valueOf(valeurTempo));
    }
}
