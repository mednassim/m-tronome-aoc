package fr.istic.aoc.v2.ihm;

import fr.istic.aoc.Material.Molette;

public class ConcreteMolette implements Molette {

    private IHMImpl ihm;

    public ConcreteMolette(IHMImpl ihm) {
        this.ihm = ihm;
    }

    @Override
    public float position() {
        return (float) ihm.getSlider().getValue();
    }
}
