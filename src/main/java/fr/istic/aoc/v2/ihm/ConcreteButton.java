package fr.istic.aoc.v2.ihm;


import javafx.scene.control.Button;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import static com.sun.java.accessibility.util.AWTEventMonitor.addMouseListener;

public class ConcreteButton extends Button implements MouseListener {


    private boolean active = false;

    public ConcreteButton() {
        addMouseListener(this);
    }


    public boolean isActive() {
        return active;
    }

    public void unActive() {
        active = false;
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        active = true;
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        active = false;

    }

}
