package fr.istic.aoc.v2.ihm;


import fr.istic.aoc.Material.EmetteurSonore;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.nio.file.Paths;

public class ConcreteEmetteurSonore implements EmetteurSonore {

    public ConcreteEmetteurSonore() {
    }

    @Override
    public void emettreClic(int numLED) {
        new Thread(() -> {
            String bip = "";
            if (numLED == 1) {
                bip = "src/main/WindowsError.wav";
            } else {
                bip = "src/main/WindowsStop.wav";
            }


            Media media = new Media(Paths.get(bip).toUri().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(media);
            mediaPlayer.play();
        }).start();
    }
}
