package fr.istic.aoc.v2.ihm;

import fr.istic.aoc.Material.*;
import fr.istic.aoc.adapter.ConcreteAdapter;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;

public class IHMImpl extends AnchorPane {

    public ConcreteButton[] tabButton = new ConcreteButton[5];
    protected ConcreteButton inc;
    protected ConcreteButton dec;
    protected ConcreteButton start;
    protected ConcreteButton stop;
    protected Label screen;
    protected InnerShadow innerShadow;
    protected Slider slider;
    protected Circle redCircle;
    protected Circle greenCircle;
    protected Label label;
    protected Label label0;

    private ConcreteAdapter adapter;
    private Horloge horloge;
    private Afficheur afficheur;
    private Clavier clavier;
    private EmetteurSonore emetteurSonore;
    private Molette molette;

    public IHMImpl() {
        super();
        horloge = new HorlogeImpl();

        afficheur = new ConcreteAfficheur(this);
        clavier = new ConcreteClavier(this);
        emetteurSonore = new ConcreteEmetteurSonore();
        molette = new ConcreteMolette(this);


        inc = new ConcreteButton();
        dec = new ConcreteButton();
        start = new ConcreteButton();
        stop = new ConcreteButton();
        screen = new Label();
        innerShadow = new InnerShadow();
        slider = new Slider();
        redCircle = new Circle();
        greenCircle = new Circle();
        label = new Label();
        label0 = new Label();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(203.0);
        setPrefWidth(593.0);

        inc.setLayoutX(310.0);
        inc.setLayoutY(126.0);
        inc.setMnemonicParsing(false);
        inc.setPrefHeight(35.0);
        inc.setPrefWidth(86.0);
        inc.setText("INC");

        dec.setLayoutX(424.0);
        dec.setLayoutY(126.0);
        dec.setMnemonicParsing(false);
        dec.setPrefHeight(35.0);
        dec.setPrefWidth(86.0);
        dec.setText("DEC");

        start.setLayoutX(73.0);
        start.setLayoutY(126.0);
        start.setMnemonicParsing(false);
        start.setPrefHeight(35.0);
        start.setPrefWidth(86.0);
        start.setText("START");

        stop.setLayoutX(188.0);
        stop.setLayoutY(126.0);
        stop.setMnemonicParsing(false);
        stop.setPrefHeight(35.0);
        stop.setPrefWidth(86.0);
        stop.setText("STOP");

        screen.setAlignment(javafx.geometry.Pos.CENTER);
        screen.setContentDisplay(javafx.scene.control.ContentDisplay.CENTER);
        screen.setLayoutX(249.0);
        screen.setLayoutY(56.0);
        screen.setNodeOrientation(javafx.geometry.NodeOrientation.LEFT_TO_RIGHT);
        screen.setPrefHeight(46.0);
        screen.setPrefWidth(96.0);
        screen.setText("20");
        screen.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        screen.setTextOverrun(javafx.scene.control.OverrunStyle.WORD_ELLIPSIS);
        screen.setFont(new Font(33.0));

        innerShadow.setColor(javafx.scene.paint.Color.valueOf("#ae1919"));
        screen.setEffect(innerShadow);

        slider.setBlockIncrement(100);
        slider.setLayoutX(73.0);
        slider.setLayoutY(71.0);
        slider.setMax(180);
        slider.setMin(20);
        slider.setPrefHeight(15.0);
        slider.setPrefWidth(150);

        redCircle.setFill(javafx.scene.paint.Color.valueOf("#ffffff"));
        redCircle.setLayoutX(438.0);
        redCircle.setLayoutY(64.0);
        redCircle.setRadius(8.0);
        redCircle.setStroke(javafx.scene.paint.Color.BLACK);
        redCircle.setStrokeType(javafx.scene.shape.StrokeType.INSIDE);

        greenCircle.setFill(javafx.scene.paint.Color.valueOf("#ffffff"));
        greenCircle.setLayoutX(438.0);
        greenCircle.setLayoutY(94.0);
        greenCircle.setRadius(8.0);
        greenCircle.setStroke(javafx.scene.paint.Color.BLACK);
        greenCircle.setStrokeType(javafx.scene.shape.StrokeType.INSIDE);

        label.setLayoutX(466.0);
        label.setLayoutY(56.0);
        label.setText("LED 1");

        label0.setLayoutX(467.0);
        label0.setLayoutY(86.0);
        label0.setText("LED 2");

        getChildren().add(inc);
        getChildren().add(dec);
        getChildren().add(start);
        getChildren().add(stop);
        getChildren().add(screen);
        getChildren().add(slider);
        getChildren().add(redCircle);
        getChildren().add(greenCircle);
        getChildren().add(label);
        getChildren().add(label0);

        tabButton[1] = start;
        tabButton[2] = stop;
        tabButton[3] = inc;
        tabButton[4] = dec;


        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            slider.setValue(newValue.intValue());


        });

    }

    public Circle getLed1() {
        return redCircle;
    }

    public Circle getLed2() {
        return greenCircle;
    }


    public Label getScreen() {
        return screen;
    }

    public Slider getSlider() {
        return slider;
    }

    public void setAdapter(ConcreteAdapter adapter) {
        this.adapter = adapter;
    }


    public Clavier getClavier() {
        return this.clavier;
    }


    public Molette getMolette() {
        return this.molette;
    }


    public EmetteurSonore getEmetteurSonore() {
        return this.emetteurSonore;
    }


    public Afficheur getAfficheur() {
        return this.afficheur;
    }


    public Horloge getHorloge() {
        return this.horloge;
    }


}
