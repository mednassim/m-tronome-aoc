package fr.istic.aoc.adapter;


import fr.istic.aoc.Controller.Controller;
import fr.istic.aoc.Material.Horloge;
import fr.istic.aoc.v1.ihm.ControllerIHM;
import fr.istic.aoc.v2.ihm.IHMImpl;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ConcreteAdapter implements ControllerIHM, Controller {

    private Controller controller;
    private IHMImpl ihm;

    private Horloge horloge;

    private boolean btnStart;
    private boolean btnStop;
    private boolean btnPlus;
    private boolean btnMinus;
    private Timer timer = new Timer();
    private TimerTask timerTask1;

    private float tempo;

    public void lireMateriel() {
        timerTask1 = new TimerTask() {
            @Override
            public void run() {
                boolean oldBtnStart = btnStart;
                btnStart = ihm.getClavier().touchePressée(1);
                if ((btnStart != oldBtnStart) && btnStart) {
                    controller.start();
                }


                boolean oldBtnStop = btnStop;
                btnStop = ihm.getClavier().touchePressée(2);
                if ((btnStop != oldBtnStop) && btnStop) {
                    controller.stop();
                }

                boolean oldBtnPlus = btnPlus;
                btnPlus = ihm.getClavier().touchePressée(3);
                if ((btnPlus != oldBtnPlus) && btnPlus) {
                    controller.inc();
                }

                boolean oldBtnMinus = btnMinus;
                btnMinus = ihm.getClavier().touchePressée(4);
                if ((btnMinus != oldBtnMinus) && btnMinus) {
                    controller.dec();
                }

                float oldtempo = tempo;
                tempo = ihm.getMolette().position();
                System.out.println(tempo);
                //}
                if (tempo != oldtempo) {
                    controller.start();
                }
            }
        };
        timer.schedule(timerTask1, new Date(), (long) (1));
        ihm.getSlider().valueProperty().addListener((observable, oldValue, newValue) -> {
            ihm.getSlider().setValue(newValue.intValue());

            ihm.getAfficheur().afficherTempo((int) ihm.getSlider().getValue());
            stop();
            start();
        });


    }

    @Override
    public void dec() {
        controller.dec();
    }

    @Override
    public void inc() {
        controller.inc();
    }

    @Override
    public void start() {
        controller.start();
    }

    @Override
    public void stop() {
        controller.stop();
    }

    @Override
    public void flashLed(int numLED) {
        controller.flashLed(numLED);
    }


    @Override
    public void setIHM(ControllerIHM ihm) {
        this.ihm = (IHMImpl) ihm;
    }

    public void setIHM(IHMImpl ihm) {
        this.ihm = ihm;
        lireMateriel();
    }

    @Override
    public void signalLed(int numLED) {
        ihm.getAfficheur().allumerLED(numLED);
    }

    @Override
    public Circle getLed1() {
        return ihm.getLed1();
    }

    @Override
    public Circle getLed2() {
        return ihm.getLed2();
    }

    @Override
    public Slider getSlider() {
        return ihm.getSlider();
    }

    @Override
    public float getSliderPosition() {
        return ihm.getMolette().position();
    }

    @Override
    public Label getScreen() {
        return ihm.getScreen();
    }


    public void sonner() {
        ihm.getEmetteurSonore().emettreClic(2);
    }

    public void flasherLED(boolean mesure) {
        ihm.getAfficheur().allumerLED(1);
        if (mesure) {
            ihm.getAfficheur().allumerLED(2);
        }
    }

    public Controller getController() {
        return this.controller;
    }


    public void setController(Controller controller) {
        this.controller = controller;
    }


}
