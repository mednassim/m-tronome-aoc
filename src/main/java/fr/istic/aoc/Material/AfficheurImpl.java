package fr.istic.aoc.Material;

import fr.istic.aoc.v1.ihm.ControllerIHM;
import javafx.animation.FillTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class AfficheurImpl implements Afficheur {

    private ControllerIHM controllerIHM;

    public AfficheurImpl(ControllerIHM controllerIHM) {
        this.controllerIHM = controllerIHM;
    }

    @Override
    public void allumerLED(int numLED) {
        new Thread(() -> {
            if (numLED == 1) {
                FillTransition st = new FillTransition(Duration.millis(100), controllerIHM.getLed1(),
                        Color.RED, Color.WHITE);
                st.play();
            } else {
                FillTransition st = new FillTransition(Duration.millis(100), controllerIHM.getLed2(),
                        Color.GREEN, Color.WHITE);
                st.play();
            }
        }).start();
    }


    @Override
    public void afficherTempo(int valeurTempo) {
        System.out.println(String.valueOf(valeurTempo));
        controllerIHM.getScreen().setText(String.valueOf(valeurTempo));
    }


}
