package fr.istic.aoc.Material;

import fr.istic.aoc.Command.Command;

public interface Horloge {

    void activerPeriodiquement(Command cmd, float périodeEnSecondes);

    void activerApresDelai(Command cmd, float délaiEnSecondes);

    void desactiver();

}