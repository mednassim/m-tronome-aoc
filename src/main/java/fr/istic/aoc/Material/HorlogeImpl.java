package fr.istic.aoc.Material;

import fr.istic.aoc.Command.Command;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class HorlogeImpl implements Horloge {

    private Timer timer = new Timer();

    private TimerTask timerTask1;

    private TimerTask timerTask2;

    public HorlogeImpl() {

    }

    public void activerPeriodiquement(final Command cmd, final float periodeEnSecondes) {
        timerTask1 = new TimerTask() {
            @Override
            public void run() {
                cmd.execute();
            }
        };
        timer.schedule(timerTask1, new Date(), (long) (periodeEnSecondes));
    }

    public void activerApresDelai(final Command cmd, final float delaiEnSecondes) {
        timerTask2 = new TimerTask() {
            @Override
            public void run() {
                cmd.execute();
            }
        };
        timer.schedule(timerTask2, new Date(), (long) (delaiEnSecondes));
    }

    public void desactiver() {
        if (timerTask1 != null && timerTask2 != null) {
            timerTask1.cancel();
            timerTask2.cancel();
        }

    }
}
