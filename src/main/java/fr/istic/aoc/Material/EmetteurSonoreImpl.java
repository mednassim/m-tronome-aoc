package fr.istic.aoc.Material;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.nio.file.Paths;

public class EmetteurSonoreImpl implements EmetteurSonore {

    public EmetteurSonoreImpl() {
    }

    @Override
    public void emettreClic(int numLED) {
        new Thread(() -> {
            String bip = "";
            if (numLED == 1) {
                bip = "src/main/java/fr/istic/aoc/Resources/WindowsError.wav";
            } else {
                bip = "src/main/java/fr/istic/aoc/Resources/WindowsStop.wav";
            }

            Media media = new Media(Paths.get(bip).toUri().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(media);
            mediaPlayer.play();
        }).start();
    }
}