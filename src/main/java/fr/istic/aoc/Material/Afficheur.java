package fr.istic.aoc.Material;

public interface Afficheur {

    void allumerLED(int numLED);

    void afficherTempo(int valeurTempo);

}