package fr.istic.aoc.Material;

import fr.istic.aoc.v1.ihm.ControllerIHM;

public class MoletteImpl implements Molette {

    private ControllerIHM controllerIHM;

    public MoletteImpl(ControllerIHM controllerIHM) {
        this.controllerIHM = controllerIHM;
    }

    @Override
    public float position() {
        return (float) controllerIHM.getSlider().getValue();
    }
}
