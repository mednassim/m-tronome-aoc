package fr.istic.aoc.Material;

public interface Materiel {

    Horloge getHorloge();

    Clavier getClavier();

    Molette getMolette();

    EmetteurSonore getEmetteurSonore();

    Afficheur getAfficheur();

}