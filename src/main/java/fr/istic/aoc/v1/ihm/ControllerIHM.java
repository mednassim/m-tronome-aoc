package fr.istic.aoc.v1.ihm;

import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;

public interface ControllerIHM {

    public void dec();

    public void inc();

    public void start();

    public void stop();

    public void signalLed (int numLED);

    public Circle getLed1();

    public  Circle getLed2();

    public Slider getSlider();

    public float getSliderPosition();

    public Label getScreen ();
}
