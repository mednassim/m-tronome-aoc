package fr.istic.aoc.v1.ihm;

import fr.istic.aoc.Command.*;
import fr.istic.aoc.Controller.Controller;
import fr.istic.aoc.Controller.ControllerImpl;
import fr.istic.aoc.Material.*;
import fr.istic.aoc.Moteur.Engine;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerIHMImpl implements ControllerIHM, Initializable {

    Engine engine;

    @FXML
    private Circle redCircle;
    @FXML
    private Circle greenCircle;
    @FXML
    private Slider slider;

    @FXML
    private Label screen;

    private Controller controller;

    private Afficheur afficheur;

    private EmetteurSonore emetteurSonore;

    private Molette molette;


    public ControllerIHMImpl() {


    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.controller = new ControllerImpl();
        controller.setIHM(this);
        afficheur = new AfficheurImpl(this);
        emetteurSonore = new EmetteurSonoreImpl();
        slider.setValue(20);
        molette = new MoletteImpl(this);

        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            slider.setValue(newValue.intValue());

            afficheur.afficherTempo((int) slider.getValue());
            stop();
            start();
        });
    }

    public void dec() {
        Command dec = new Dec(controller);
        dec.execute();
    }


    public void inc() {
        Command inc = new Inc(controller);
        inc.execute();
    }

    public void start() {
        Command start = new Start(controller);
        start.execute();
    }


    public void stop() {
        Command stop = new Stop(controller);
        stop.execute();

    }

    public void signalLed(int numLED) {
        afficheur.allumerLED(numLED);
        emetteurSonore.emettreClic(numLED);
    }

    public Circle getLed1() {
        return redCircle;
    }

    public Circle getLed2() {
        return greenCircle;
    }

    public float getSliderPosition() {
        return molette.position();
    }

    public Slider getSlider() {
        return this.slider;
    }

    public Label getScreen() {
        return screen;
    }


}
