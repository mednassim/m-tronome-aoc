package fr.istic.aoc.Command;

import fr.istic.aoc.Controller.Controller;

public class Inc implements Command {

	private Controller controller;

	public Inc (Controller controller){
		this.controller=controller;
	}

	public void execute() {
		controller.inc();
	}

}
