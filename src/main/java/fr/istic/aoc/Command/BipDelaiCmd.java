package fr.istic.aoc.Command;

import fr.istic.aoc.Controller.Controller;

/**
 * Command de battement après delais
 */
public class BipDelaiCmd implements Command {

    Controller controller;

    /**
     * constructeur
     *
     * @param controller controleur du metronome
     */
    public BipDelaiCmd(Controller controller) {
        this.controller = controller;
    }

    /**
     * la methode execute de l'interface Command
     */
    public void execute() {
        controller.flashLed(2);
    }
}
