package fr.istic.aoc.Command;


import fr.istic.aoc.Controller.Controller;

/**
 * la commande decremente mesure
 */
public class Dec implements Command {

    private Controller controller;

    public Dec(Controller controller) {
        this.controller = controller;
    }

    public void execute() {

        controller.dec();
    }

}
