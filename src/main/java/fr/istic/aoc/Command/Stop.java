package fr.istic.aoc.Command;


import fr.istic.aoc.Controller.Controller;

/**
 * la commande stop du metronome
 */
public class Stop implements Command {

	private Controller controller;

	public Stop (Controller controller){
		this.controller=controller;
	}
	public void execute() {

		controller.stop();
	}

}
