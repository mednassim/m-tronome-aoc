package fr.istic.aoc.Command;

/**
 * interfcae command du pattern command
 */
public interface Command {
    void execute();
}