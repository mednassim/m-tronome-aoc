package fr.istic.aoc.Command;


import fr.istic.aoc.Controller.Controller;

/**
 * la commande du bouton start
 * lance le metronome
 */
public class Start implements Command {

	private Controller controller;

	public Start (Controller controller){
		this.controller=controller;
	}


	public void execute() {
		this.controller.start();

	}
}
