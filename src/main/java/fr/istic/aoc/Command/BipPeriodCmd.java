package fr.istic.aoc.Command;

import fr.istic.aoc.Controller.Controller;

public class BipPeriodCmd implements Command {

    Controller controller;

    public BipPeriodCmd(Controller controller) {
        this.controller = controller;
    }

    public void execute() {
        controller.flashLed(1);
    }
}
