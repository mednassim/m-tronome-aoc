package fr.istic.aoc.Moteur;

import fr.istic.aoc.Command.Command;
import fr.istic.aoc.Material.Horloge;

public interface Engine {
    public boolean isRunning();

    public void setRunning(boolean running);

    public int getBpm();

    public void setBpm(int bpm);

    public Horloge getHorloge();

    public void startBip(Command cmd);

    public void startMesure(Command cmd);

    public void decMesure();

    public void incMesure();

    public void setMesure(int mesure);

}
