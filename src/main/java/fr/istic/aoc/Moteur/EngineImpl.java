package fr.istic.aoc.Moteur;


import fr.istic.aoc.Command.Command;
import fr.istic.aoc.Material.Horloge;
import fr.istic.aoc.Material.HorlogeImpl;

public class EngineImpl implements Engine {

    private Horloge horloge;

    private int mesure;

    private int bpm;

    private boolean running = false;

    public EngineImpl() {
        this.horloge = new HorlogeImpl();
        ;
    }

    public void setMesure(int mesure) {
        this.mesure = mesure;
    }

    public void incMesure() {
        if (mesure < 7) {
            this.mesure++;
        }
    }

    public void decMesure() {
        if (mesure > 2) {
            this.mesure--;

        }
    }

    public int getBpm() {
        return bpm;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public Horloge getHorloge() {
        return horloge;
    }

    public void startBip(Command cmd) {
        this.horloge.activerPeriodiquement(cmd, this.bpm);
    }

    public void startMesure(Command cmd) {
        this.horloge.activerApresDelai(cmd, this.bpm * mesure);
    }
}
